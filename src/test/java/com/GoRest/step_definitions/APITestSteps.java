package com.GoRest.step_definitions;

import com.GoRest.Pojo.Datum;
import com.GoRest.Pojo.GoRestPojo;
import com.GoRest.Pages.ApiClient;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class APITestSteps {

    private int id;

    @Given("As a user I should get all of the data id values are four digit integers and none of them are null")
    public void as_a_user_i_should_get_all_of_the_data_id_values_are_four_digit_integers_and_none_of_them_are_null() {
        String endPoint = "/users"; // Doğru endpoint olduğundan emin olun
        Response response = ApiClient.get(endPoint);


        assertEquals("Expected HTTP status 200", 200, response.statusCode());


        assertEquals("Unexpected content type", "application/json; charset=utf-8", response.contentType());


        GoRestPojo goRest = response.body().as(GoRestPojo.class);
        List<Datum> datumList = goRest.getData();


        for (Datum datum : datumList) {
            assertNotNull("ID should not be null for datum: " + datum, datum.getId());

            int id = datum.getId();
            assertTrue("ID should be a 4-digit integer but was: " + id, id >= 1000 && id <= 9999999);
        }
    }


    @When("As a user I should create a data with given info")
    public void as_a_user_i_should_create_a_data_with_given_info() {

        String jsonBody = "{\n" +
                "    \"email\": \"jdkl@gmail.com\",\n" +
                "    \"name\": \"test\",\n" +
                "    \"gender\": \"male\",\n" +
                "    \"status\": \"active\"\n" +
                "}";

        String endPoint = "/users";
        Response response = ApiClient.post(endPoint, jsonBody);
        assertEquals(201, response.statusCode());
        id = response.jsonPath().getInt("data.id");
    }

    @Then("As a user I should not create a data with used info")
    public void user_should_not_create_a_data_with_used_info() {

        String jsonBody = "{\n" +
                "    \"email\": \"jdkl@gmail.com\",\n" +
                "    \"name\": \"test\",\n" +
                "    \"gender\": \"male\",\n" +
                "    \"status\": \"active\"\n" +
                "}";

        String endPoint = "/users";
        Response response = ApiClient.post(endPoint, jsonBody);


        Assert.assertEquals("has already been taken", response.jsonPath().getString("data[0].message"));

// remove user
        Response deleteResponse = ApiClient.delete(endPoint + "/" + id);
        assertEquals(204, deleteResponse.statusCode());
    }
}
