package com.GoRest.Pages;

import com.GoRest.utilities.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class TechnicalDocumentsPage extends BasePage {

    @FindBy(xpath = "//span[@class='select2-selection__rendered']")
    public WebElement dropDownBox;

    @FindBy(xpath = "//input[@class='select2-search__field']")
    public WebElement dropDownSearchBox;

    // View document links
    public List<WebElement> viewThisDocs() {
        return Driver.get().findElements(By.xpath("//a[contains(text(),'View this document')]"));
    }

    // Download document links
    public List<WebElement> downloadLinks() {
        return Driver.get().findElements(By.xpath("//a[contains(text(),'Download')]"));
    }

    // Choose a language label
    @FindBy(xpath = "//label[contains(text(), 'Choose a language')]/following-sibling::b")
    public WebElement chooseLanguageLabel;

    // Language option selection (text inside li[role="treeitem"])
    @FindBy(id= "select2-id_language-container")
    public WebElement languageOption;

    @FindBy(xpath = "//input[@class='select2-search__field']")
    public WebElement dropDownLanguageBox;

    // Dropdown container element for doc number
    @FindBy(id = "select2-id_type_number-container")
    public WebElement docNumberDropDownContainer;

    // Doc number option selection (text inside li[role="treeitem"])
    @FindBy(xpath = "//li[@role='treeitem' and contains(text(), 'docNumber')]")
    public WebElement docNumberOption;


    @FindBy(xpath = "//h3[@class='result-item-title']")
    public WebElement itemName;

    @FindBy(xpath = "//dd[@class='detail-value detail-value__type-number']")
    public WebElement typeNumber;









}
