package com.GoRest.Pages;


import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class ApiClient {

    private static final String BASE_URL = "https://gorest.co.in/public/v1";
    private static final String AUTHORIZATION_HEADER = "Bearer 1db9c9b6c959682be7c96f74ca532c3cb0bd331f46b86a92602f8d319481b6f5";

    // GET isteği
    public static Response get(String endpoint) {
        return RestAssured.given()
                .baseUri(BASE_URL)
                .header("Authorization", AUTHORIZATION_HEADER)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get(endpoint);
    }

    // POST isteği
    public static Response post(String endpoint, String jsonBody) {
        return RestAssured.given()
                .baseUri(BASE_URL)
                .header("Authorization", AUTHORIZATION_HEADER)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(jsonBody)
                .when()
                .post(endpoint);
    }

    public static Response delete(String endpoint) {
        return RestAssured.given()
                .baseUri(BASE_URL)
                .header("Authorization", AUTHORIZATION_HEADER)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .delete(endpoint);
    }
}

